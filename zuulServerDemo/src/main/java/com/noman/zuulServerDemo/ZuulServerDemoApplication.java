package com.noman.zuulServerDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulServerDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulServerDemoApplication.class, args);
	}

}
